# Magic Compass

Teleport system for Minetest (yes, GUI aspect will be improved)  
  
<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

<img src="https://content.minetest.net/uploads/pt68Gn7BjL.png"/>  

### How to use it
Look for the "Magic Compass" item in your creative inventory, put it in your hand and left click it to open the locations menu

### Customisation
Check out the [DOCS](/DOCS.md) to create new locations, use callbacks and change the graphic aspect of your compass

### Want to help?
Feel free to:
* open an [issue](https://gitlab.com/zughy-friends-minetest/magic-compass/-/issues)
* submit a merge request. In this case, PLEASE, do follow milestones and my [coding guidelines](https://cryptpad.fr/pad/#/2/pad/view/-l75iHl3x54py20u2Y5OSAX4iruQBdeQXcO7PGTtGew/embed/). I won't merge features for milestones that are different from the upcoming one (if it's declared), nor messy code
* contact me on the [Minetest Forum](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=26472)

---

Images are under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
