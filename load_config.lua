local config_file = io.open(minetest.get_modpath("magic_compass") .. "/config.txt", "r")
local data = string.split(config_file:read("*all"), "\n")

magic_compass.menu_title = string.match(data[1], "=(.*)")
magic_compass.menu_gui_bg_2rows = string.match(data[3], "=(.*)")
magic_compass.menu_gui_bg_4rows = string.match(data[4], "=(.*)")
magic_compass.menu_gui_bg_6rows = string.match(data[5], "=(.*)")
magic_compass.menu_gui_button_bg = string.match(data[6], "=(.*)")

config_file:close()
